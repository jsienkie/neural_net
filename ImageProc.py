import cv2
import numpy as np
import math
import re
from PyQt5.QtGui import *
from nn import *


class ImageProc:

    LONGEST_LINES = 7
    IMAGE_DIM = 300
    GESTURES = 3

    def __init__(self, labels,gesture_label, frame=None):
        self.nn = NeuralNetwork(ImageProc.LONGEST_LINES)
        self.gesture_label = gesture_label
        self.frame = frame
        self.orig_frame = frame
        self.process_image_f_list = \
            [self.to_gray, self.to_close, self.to_canny, self.to_lines]

        self.labels = labels
        self.trained = False

    def train(self, files):
        self.trained = False
        lines_list = []
        outputs_list = []

        try:
            for f in files:
                self.set_frame(cv2.imread(f))
                lines_list.append(self.lines)
                gest_number = ImageProc.set_output(int(re.search("[0-9]", f).group(0)))
                outputs_list.append(gest_number)
            self.nn.train_data(lines_list, outputs_list)
        except Exception as e :
            print(e)
        self.trained = True

    @staticmethod
    def set_output(out):
        output = [0] * ImageProc.GESTURES
        output[out - 1] = 1
        return output

    def set_frame(self, frame):
        self.frame = frame
        self.orig_frame = frame
        self.process_image()
        if self.trained:
            gesture_index = self.sim()
            self.gesture_label.setText(str(gesture_index))
        return -1

    def sim(self):
        return self.nn.sim(self.lines)

    def process_image(self, train = False):
        try:
            for i in range(len(self.labels)):
                q_image = self.process_image_f_list[i]()
                if not train:
                    self.labels[i].setPixmap(QPixmap.fromImage(q_image))
        except Exception as e:
            print(e)

    def get_lines(self):
        return self.lines

    @staticmethod
    def to_q_image(frame):
        height, width = frame.shape
        return QImage(frame, width, height, QImage.Format_Indexed8)

    def to_gray(self):
        self.frame = cv2.resize(self.frame, (ImageProc.IMAGE_DIM, ImageProc.IMAGE_DIM), interpolation=cv2.INTER_CUBIC)
        self.frame = cv2.cvtColor(self.frame, cv2.COLOR_RGB2GRAY, self.frame)
        self.orig_frame = self.frame

        return self.to_q_image(self.frame)

    def to_close(self):
        kernel = np.ones((5, 5), np.uint8)
        self.frame = cv2.morphologyEx(self.frame, cv2.MORPH_CLOSE, kernel)

        return self.to_q_image(self.frame)

    def to_canny(self):
        self.frame = cv2.Canny(self.frame, threshold1=50, threshold2=100, apertureSize=3)

        return self.to_q_image(self.frame)

    def to_lines(self):
        self.lines = cv2.HoughLinesP(self.frame, rho=1, theta=np.pi / 180, threshold=20,
                                minLineLength=30, maxLineGap=10)

        self.lines = ImageProc.get_longest_lines(self.lines)

        for i in range(len(self.lines)):
            cv2.line(self.orig_frame,
                     (self.lines[i][0], self.lines[i][1]),
                     (self.lines[i][2], self.lines[i][3]),
                     (0, 255, 0), 3, 4
                     )
        self.lines = self.lines.flatten()
        return self.to_q_image(self.orig_frame)

    @staticmethod
    def get_longest_lines(lines):
        if lines is None:
            lines = np.array([])
        else:
            lines = sorted(lines, key=lambda
                x: math.sqrt(pow(x[0][0] - x[0][2], 2) + pow(x[0][1] - x[0][3], 2))
                )
            lines = np.array(lines)
        lines.resize(ImageProc.LONGEST_LINES,4,refcheck=False)
        return lines[-ImageProc.LONGEST_LINES:]
