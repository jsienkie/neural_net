import neurolab as nl
import numpy as np

# Create train samples


class NeuralNetwork:

    def __init__(self, lines):
        pass

    def train_data(self, lines, output):
        # Create network with 2 layers and random initialized
        input_format = [[0, 300]]*7*4
        self.net = nl.net.newff(input_format, [40, 3])

        # Train network
        error = self.net.train(lines, output, epochs=500, show=10, goal=0.02)
        print(error)
        # Simulate network
        out = self.net.sim(lines)
        print(out)

    def sim(self, lines):
        out = self.net.sim([lines])
        return np.argmax(out) + 1

