from gui import *
from VideoController import *
import glob
import os.path

class Controller():

    NN_FILE = "nn.txt"

    def __init__(self):
        self.main_window = MyDialog()
        labels = [ self.main_window.main_layout.images_layout.imgOrigLabel,
            self.main_window.main_layout.images_layout.imgLabel2,
            self.main_window.main_layout.images_layout.imgLabel3,
            self.main_window.main_layout.images_layout.imgLabel4]

        gesture_label = self.main_window.main_layout.option_layout.gesture_text

        self.image_proc = ImageProc(labels, gesture_label)

        self.video_controller = VideController(self.image_proc)
        self.main_window.main_layout.option_layout.button_capture.clicked.connect(self.video_controller.setup_trigger)
        self.setup_train_data()

    def on_save_nn(self):
        self.image_proc.nn.net.save(Controller.NN_FILE)

    def on_load_nn(self):
        if not os.path.isfile(Controller.NN_FILE):
            print("no nn file")
            return
        self.image_proc.nn.net.load(Controller.NN_FILE)
        self.set_buttons(True)

    def setup_train_data(self):
        files = glob.glob("./imgs/*.jpg")
        menu = self.main_window.main_layout.option_layout.combobox

        for f in files:
            menu.addItem(f)

        menu.currentIndexChanged.connect(self.on_item_select)

        button_train = self.main_window.main_layout.option_layout.button_train
        button_train.clicked.connect(self.on_train_clicked)

        self.main_window.main_layout.option_layout.button_save_train.clicked.connect(self.on_save_nn)
        self.main_window.main_layout.option_layout.button_load_train.clicked.connect(self.on_load_nn)

        self.set_buttons(True)

    def on_item_select(self):
        menu = self.main_window.main_layout.option_layout.combobox
        gesture_label = self.main_window.main_layout.option_layout.gesture_text
        cb = menu.currentText()
        img = cv2.imread(cb)
        self.image_proc.set_frame(img)


    def on_train_clicked(self):
        files = glob.glob("./imgs/t*.jpg")
        self.image_proc.train(files)
        self.set_buttons(True)

    def set_buttons(self, enable):
        self.main_window.main_layout.option_layout.combobox.setEnabled(enable)
        self.main_window.main_layout.option_layout.button_capture.setEnabled(enable)
        self.main_window.main_layout.option_layout.button_save_train.setEnabled(enable)

    def get_main_window(self):
        return self.main_window

