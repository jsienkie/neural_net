
from Controller import *


class MainGestRecog:

    def __init__(self):
        import sys

        app = QApplication(sys.argv)
        c = Controller()
        w = c.get_main_window()
        w.show()

        app.exec_()


if __name__ == "__main__":
    gest_recog = MainGestRecog()