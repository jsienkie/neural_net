
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *


class ImagesLayout(QHBoxLayout):

    def __init__(self, parent = None):
        super(QHBoxLayout, self).__init__(parent)

        self.imgOrigLabel = QLabel()
        self.imgLabel2 = QLabel()
        self.imgLabel3 = QLabel()
        self.imgLabel4 = QLabel()

        self.addWidget(self.imgOrigLabel)
        self.addWidget(self.imgLabel2)
        self.addWidget(self.imgLabel3)
        self.addWidget(self.imgLabel4)

    def set_orig_img(self, img):
        self.imgOrigLabel.setPixmap(img)


class OptionLayout(QHBoxLayout):
    def __init__(self, parent = None):
        super(QHBoxLayout, self).__init__(parent)
        self.button_capture = QPushButton()
        self.button_save_train = QPushButton()
        self.button_load_train = QPushButton()
        self.button_train = QPushButton()
        self.gesture_text = QLabel()

        self.button_capture.setText("capture")
        self.button_train.setText("train")
        self.gesture_text.setText("-1")
        self.button_save_train.setText("Save")
        self.button_load_train.setText("Load")

        self.combobox = QComboBox()

        self.addWidget(self.button_capture)
        self.addWidget(self.button_train)
        self.addWidget(self.gesture_text)
        self.addWidget(self.combobox)
        self.addWidget(self.button_save_train)
        self.addWidget(self.button_load_train)


class MainLayout(QVBoxLayout):
    def __init__(self, parent=None):
        super(QVBoxLayout, self).__init__(parent)

        self.images_layout = ImagesLayout()
        self.option_layout = OptionLayout()

        self.addLayout(self.images_layout)
        self.addLayout(self.option_layout)


class MyDialog(QMainWindow):

    def __init__(self, parent=None):
        super(QMainWindow, self).__init__(parent)

        self.main_layout = MainLayout()

        self.window = QWidget()
        self.window.setLayout(self.main_layout)
        self.setCentralWidget(self.window)

