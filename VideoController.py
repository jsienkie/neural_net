
from PyQt5.QtCore import *
from ImageProc import *


class VideController:

    def __init__(self, image_proc):
        self.image_proc = image_proc
        self.recording = False

        self.trigger = QTimer()
        self.trigger.timeout.connect(self.capture)
        self.cap = cv2.VideoCapture(0)

        img = cv2.imread(r'HD-Nature-Wallpapers-For-PC.jpg')
        self.image_proc.set_frame(img)

    def setup_trigger(self):
        if self.recording:
            self.trigger.stop()
        else:
            self.trigger.start(1000)

        self.recording = not self.recording

    def capture(self):
        ret, frame = self.cap.read()
        self.image_proc.set_frame(frame)